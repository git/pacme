/* plet-out.c - A pokelet for output channel.  */

/* Copyright (C) 2022-2023 Mohammad-Reza Nabipoor */

/* This file is part of pacme.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <assert.h>
#include <inttypes.h>
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <err.h>

#define ENSURE(cond, msg)                                                     \
  do                                                                          \
    {                                                                         \
      if (!(cond))                                                            \
        errx (1, "condition '%s' failed: %s", #cond, msg);                    \
    }                                                                         \
  while (0)

static void
read_n_bytes (int fd, void *mem, size_t len)
{
  ssize_t n;
  size_t off = 0;

  while (off != len)
    {
      n = read (fd, mem + off, len - off);
      if (n == 0)
        errx (1, "EOF");
      if (n == -1)
        err (1, "read() failed");
      off += n;
    }
}

static void
write_n_bytes (int fd, char *mem, size_t len)
{
  ssize_t n;
  size_t off = 0;

  while (off != len)
    {
      n = write (fd, mem + off, len - off);
      if (n == -1)
        err (1, "write() failed");
      off += n;
    }
}

static int
poke_connect (const char *path, uint8_t role)
{
  int fd = socket (AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un adr;

  if (fd == -1)
    err (1, "socket() failed");
  memset (&adr, 0, sizeof (adr));
  adr.sun_family = AF_UNIX;
  snprintf (adr.sun_path, sizeof (adr.sun_path), path);
  if (connect (fd, (struct sockaddr *)&adr, sizeof (adr)) == -1)
    err (1, "connect(%s) failed", path);

  if (write (fd, &role, 1) != 1)
    err (1, "write(role) failed");

  return fd;
}

#define OUTKIND_ITER_BEGIN 1
#define OUTKIND_ITER_END 2
#define OUTKIND_ERR 3
#define OUTKIND_TEXT 4
#define OUTKIND_CLS_BEGIN 5
#define OUTKIND_CLS_END 6
#define OUTKIND_EVAL 7

static void
out_chan_1 (char *mem, size_t len, int no_cls_p, const char* iter_prefix,
            int iter_prefix_num_p)
{
  static size_t i = 0;
  static const char *nl = "";
  static uint64_t n_iteration = 0;

  assert (iter_prefix != NULL);

  if (mem[0] == OUTKIND_ITER_BEGIN)
    {
      ENSURE (len >= /*maybe one more extra '\0'*/ 1 + 8,
              "inconsistency in ITER_BEGIN");

      uint8_t *p = (uint8_t *)mem + 1;
      uint64_t idx = (uint64_t)p[7] << 56 | (uint64_t)p[6] << 48
                     | (uint64_t)p[5] << 40 | (uint64_t)p[4] << 32
                     | (uint64_t)p[3] << 24 | (uint64_t)p[2] << 16
                     | (uint64_t)p[1] << 8 | (uint64_t)p[0] << 0;
      ENSURE (n_iteration != idx,
              "inconsistency in iteration number (ITER_BEGIN)");
      n_iteration = idx;
      printf ("%s", iter_prefix);
      if (iter_prefix_num_p)
        printf ("%" PRIu64 "\n", idx); /* FIXME remove '\n'.  */
      nl = "";
    }
  else if (mem[0] == OUTKIND_TEXT || mem[0] == OUTKIND_EVAL)
    {
      int text_p = mem[0] == OUTKIND_TEXT;

      --len;
      ++mem;
      nl = len > 1 && mem[len - 2] == '\n' ? "" : "\n";
      printf ("%s%.*s", text_p || no_cls_p ? "" : "* ", (int)len, mem);
      --mem;
    }
  else if (mem[0] == OUTKIND_ITER_END)
    {
      ENSURE (len >= /*maybe one more extra '\0'*/ 1 + 8,
              "inconsistency in ITER_END");

      uint8_t *p = (uint8_t *)mem + 1;
      uint64_t idx = (uint64_t)p[7] << 56 | (uint64_t)p[6] << 48
                     | (uint64_t)p[5] << 40 | (uint64_t)p[4] << 32
                     | (uint64_t)p[3] << 24 | (uint64_t)p[2] << 16
                     | (uint64_t)p[1] << 8 | (uint64_t)p[0] << 0;
      if (n_iteration == 0)
        n_iteration = idx;
      else
        ENSURE (n_iteration == idx,
                "inconsistency in iteration number (ITER_END)");
      printf (nl);
    }
  else if (mem[0] == OUTKIND_CLS_BEGIN)
    {
      --len;
      ++mem;
      if (!no_cls_p)
        {
          printf ("<%s>", mem);
          nl = "\n";
        }
      --mem;
    }
  else if (mem[0] == OUTKIND_CLS_END)
    {
      --len;
      ++mem;
      if (!no_cls_p)
        {
          printf ("</%s>", mem);
          nl = "\n";
        }
      --mem;
    }
  else if (mem[0] == OUTKIND_ERR)
    {
      --len;
      ++mem;
      nl = len > 1 && mem[len - 2] == '\n' ? "" : "\n";
      printf ("(error) %.*s", (int)len, mem);
      --mem;
    }
  fflush (stdout);
  ++i;
}

static void
usage (int new_line_p)
{
  if (new_line_p)
    putchar ('\n');
  puts ("Usage: plet-out [-h] [-r] [-C] [-c <CHAN>]");
  puts ("\nOptions:");
  puts ("  -h         Show this help message.");
  puts ("  -r         Raw mode (write the whole received payload (including");
  puts ("             the 2-byte length prefix) to stdout.");
  puts ("  -L         Exclude the 2-byte length prefix in non-out channel.");
  puts ("  -C         Show Poke output classes.");
  puts ("  -c <CHAN>  Target channel number to send data (0 < CHAN && "
        "CHAN < 128).");
  puts ("  -i <fmt>   Specify the byte sequence to be printed in the "
        "beginning");
  puts ("             of the new iteration (default: '//--- %I'.");

  /* Maybe `-I <fmt>' for byte-sequence at the end of iteration.  */
}

int
main (int argc, char *argv[])
{
  int fd;
  uint8_t role = 0x81;
  int no_cls_p = 1;
  int raw_p = 0;
  int no_len_p = 0;
  char *iter_prefix
      = NULL; /* This string will be printed for each new iteration.  */
  int iter_prefix_num_p = 1; /* Append the iteration number to iter_prefix.  */

  {
    int opt;

    while ((opt = getopt (argc, argv, "hrc:Ci:rL")) != -1)
      switch (opt)
        {
        case 'r':
          raw_p = 1;
          break;
        case 'c':
          {
            long chan = strtol (optarg, NULL, 10);

            if (!(0 < chan && 0 < 128))
              errx (1, "invalid chan"); // FIXME use a better error msg
            role = 0x80 | (uint8_t)chan;
          }
          break;
        case 'C':
          no_cls_p = 0;
          break;
        case 'i':
          {
            char *fmt = strdup (optarg);
            char *pr = NULL; /* Pointer to read position.  */
            char *pw = NULL; /* Pointer to write position.  */

            if (fmt == NULL)
              err (1, "strdup() failed");

            iter_prefix_num_p = 0;

            {
              size_t len = strlen (optarg);

              /* regexp: /[^%]%I$/ */
              if ((len == 2 || (len > 2 && fmt[len - 3] != '%'))
                  && fmt[len - 2] == '%' && fmt[len - 1] == 'I')
                {
                  iter_prefix_num_p = 1;
                  fmt[len - 2] = '\0';
                  len -= 2;
                }
            }

            for (pr = fmt, pw = fmt; *pr; ++pr)
              {
                if (pr[0] == '%')
                  {
                    if (pr[1] == '\0')
                      errx (1, "invalid %%-tag");

                    if (pr[1] == '%')
                      *pw++ = *pr++;
                    else if (pr[1] == 'I')
                      errx (1, "%%I-tag is only supported at the end");
                    else
                      errx (1, "invalid %%-tag");
                  }
                else if (pr[0] == '\\')
                  {
                    if (pr[1] == '\0')
                      errx (1, "invalid escape");

                    if (pr[1] == '\\')
                      *pw++ = *pr++;
                    else if (pr[1] == 'x')
                      {
                        ENSURE (pr[2] != '\0', "invalid hex specifier");
                        ENSURE (pr[3] != '\0', "invalid hex specifier");
                        ENSURE (isxdigit (pr[2]), "invalid hex specifier");
                        ENSURE (isxdigit (pr[3]), "invalid hex specifier");

                        *pw++ = (uint8_t)(isdigit (pr[2])
                                              ? pr[2] - '0'
                                              : (pr[2] | 0x20) - 'a' + 10)
                                    << 4
                                | (uint8_t)(isdigit (pr[3])
                                                ? pr[3] - '0'
                                                : (pr[3] | 0x20) - 'a' + 10);
                        pr += 3;
                      }
                    else
                      errx (1, "invalid escape");
                  }
                else
                  *pw++ = *pr;
              }
            *pw = '\0';
            iter_prefix = fmt;
          }
          break;
        case 'L':
          no_len_p = 1;
          break;
        case 'h':
          usage (0);
          exit (0);
          break;
        default:
          usage (1);
          exit (1);
        }
    argc -= optind;
    argv += optind;

    if (iter_prefix == NULL)
      iter_prefix = strdup ("//--- ");
  }

  {
    char socket_path[32];

    snprintf (socket_path, sizeof (socket_path), "/tmp/poked-%ld.ipc",
              (long)getuid ());
    fd = poke_connect (argc > 1 ? argv[1] : socket_path, role);
    assert (fd != -1);
  }

  while (1)
    {
      uint8_t buf[2];
      size_t len;
      char *mem;

      read_n_bytes (fd, (void *)buf, 2);
      len = (size_t)buf[1] << 8 | buf[0];
      ENSURE (len > 1, "incosistency in received payload");
      mem = malloc (len);
      if (mem == NULL)
        err (1, "malloc() failed");
      read_n_bytes (fd, mem, len);
      if (!raw_p && role == 0x81)
        out_chan_1 (mem, len, no_cls_p, iter_prefix, iter_prefix_num_p);
      else
        {
          if (!no_len_p)
            write_n_bytes (1, (void *)buf, 2);
          write_n_bytes (1, mem, len);
        }
      free (mem);
    }

  free (iter_prefix);
  close (fd);
  return 0;
}
