/* plet-repl.c - A pokelet for sending data to input channels interactively. */

/* Copyright (C) 2022-2023 Mohammad-Reza Nabipoor */

/* This file is part of pacme.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <assert.h>
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <err.h>

#include <readline/history.h>
#include <readline/readline.h>

static void
write_n_bytes (int fd, char *mem, size_t len)
{
  ssize_t n;
  size_t off = 0;

  while (off != len)
    {
      n = write (fd, mem + off, len - off);
      if (n == -1)
        err (1, "write() failed");
      off += n;
    }
}

static int
poke_connect (const char *path, uint8_t role)
{
  int fd = socket (AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un adr;

  if (fd == -1)
    err (1, "socket() failed");
  memset (&adr, 0, sizeof (adr));
  adr.sun_family = AF_UNIX;
  snprintf (adr.sun_path, sizeof (adr.sun_path), path);
  if (connect (fd, (struct sockaddr *)&adr, sizeof (adr)) == -1)
    err (1, "connect(%s) failed", path);

  if (write (fd, &role, 1) != 1)
    err (1, "write(role) failed");

  return fd;
}

struct option
{
  /* Dispatch input to compiler functions similar to GNU poke CLI
     program (the `var', `type', `fun', `unit' keyword trick).  */
  int poke_compat_p;
  char *prompt; /* Prompt string for readline.  */
};

static void
usage (int new_line_p)
{
  if (new_line_p)
    putchar ('\n');
  puts ("Usage: plet-repl [-h] [-p] [-P <PROMPT>]");
  puts ("\nOptions:");
  puts ("  -h           Show this help message.");
  puts ("  -p           Use GNU poke's algorithm for choosing between "
        "code/command.");
  puts ("  -P <PROMPT>  Prompt text.");
}

static void
option_parse (struct option *opt, int argc, char *argv[])
{
  int o;

  while ((o = getopt (argc, argv, "hpP:")) != -1)
    switch (o)
      {
      case 'p':
        opt->poke_compat_p = 1;
        break;
      case 'P':
        opt->prompt = optarg;
        break;
      case 'h':
        usage (0);
        exit (0);
        break;
      default:
        usage (1);
        exit (1);
      }
  if (opt->prompt == NULL)
    opt->prompt = "#!poke!# ";
}

static char *
str_skip_whitespace (char *s)
{
  assert (s != NULL);

  for (; *s != '\0'; ++s)
    if (!isspace (*s))
      break;
  return s;
}

int
main (int argc, char *argv[])
{
  int fd_code;
  int fd_cmd;
  struct option opt;
  char socket_path[32];

  memset (&opt, 0, sizeof (opt));
  option_parse (&opt, argc, argv);
  argc -= optind;
  argv += optind;

  snprintf (socket_path, sizeof (socket_path), "/tmp/poked-%ld.ipc",
            (long)getuid ());
  fd_code = poke_connect (argc > 1 ? argv[1] : socket_path, /*code*/ 0x01);
  if (fd_code == -1)
    err (1, "connection to code channel failed");
  fd_cmd = poke_connect (argc > 1 ? argv[1] : socket_path, /*cmd*/ 0x02);
  if (fd_cmd == -1)
    err (1, "connection to command channel failed");

  rl_readline_name = "pacme";

  for (char *line; (line = readline (opt.prompt)) != NULL; free (line))
    {
      uint8_t len16le[2];
      int is_cmd_p = 0;
      size_t len;
      char *p = str_skip_whitespace (line);

      if (*p == '\0')
        continue;
      if (opt.poke_compat_p)
        {
          is_cmd_p
              = !(strncmp (p, "var", 3) == 0 || strncmp (p, "fun", 3) == 0
                  || strncmp (p, "type", 4) == 0 || strncmp (p, "unit", 4) == 0
                  || strncmp (p, "immutable", 9) == 0);
          p = line;
        }
      else
        {
          if (*p == ';')
            {
              is_cmd_p = 1;
              ++p;
            }
          else
            p = line; /* If not a command, send the original line.  */
        }

      add_history (line); /* Capture the original line.  */

      len = strlen (p);
      if (len >= 0xffff)
        {
          warnx ("cannot send the input; too big");
          continue;
        }
      p[len] = ';'; /* Replace final '\0' with ';'.  */
      ++len;
      len16le[0] = len;
      len16le[1] = len >> 8;
      write_n_bytes (is_cmd_p ? fd_cmd : fd_code, (void *)len16le, 2);
      write_n_bytes (is_cmd_p ? fd_cmd : fd_code, p, len);
    }

  close (fd_cmd);
  close (fd_code);
  return 0;
}
