/* plet-in.c - A pokelet for sending data to input channels.  */

/* Copyright (C) 2022-2023 Mohammad-Reza Nabipoor */

/* This file is part of pacme.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <err.h>

static void
write_n_bytes (int fd, char *mem, size_t len)
{
  ssize_t n;
  size_t off = 0;

  while (off != len)
    {
      n = write (fd, mem + off, len - off);
      if (n == -1)
        err (1, "write() failed");
      off += n;
    }
}

static int
poke_connect (const char *path, uint8_t role)
{
  int fd = socket (AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un adr;

  if (fd == -1)
    err (1, "socket() failed");
  memset (&adr, 0, sizeof (adr));
  adr.sun_family = AF_UNIX;
  snprintf (adr.sun_path, sizeof (adr.sun_path), path);
  if (connect (fd, (struct sockaddr *)&adr, sizeof (adr)) == -1)
    err (1, "connect(%s) failed", path);

  if (write (fd, &role, 1) != 1)
    err (1, "write(role) failed");

  return fd;
}

struct option
{
  int raw_p;
  uint8_t role;
};

static void
usage (int new_line_p)
{
  if (new_line_p)
    putchar ('\n');
  puts ("Usage: plet-in [-h] [-r] [-c <CHAN>]");
  puts ("\nOptions:");
  puts ("  -h         Show this help message.");
  puts ("  -r         Raw mode (don't add 2-byte length prefix to input "
        "data).");
  puts ("  -c <CHAN>  Target channel number to send data (0 < CHAN && "
        "CHAN < 128).");
}

static void
option_parse (struct option *opt, int argc, char *argv[])
{
  int o;

  while ((o = getopt (argc, argv, "hrc:")) != -1)
    switch (o)
      {
      case 'r':
        opt->raw_p = 1;
        break;
      case 'c':
        {
          long chan = strtol (optarg, NULL, 10);

          // FIXME improve the error message
          if (!(0 < chan && chan < 128))
            errx (1, "option_parse: invalid chan '%s'", optarg);
          opt->role = (uint8_t)chan;
        }
        break;
      case 'h':
        usage (0);
        exit (0);
        break;
      default:
        usage (1);
        exit (1);
      }

  if (!(0 < opt->role && opt->role < 128))
    opt->role = 1;
}

int
main (int argc, char *argv[])
{
  ssize_t n;
  int fd;
  enum
  {
    DATAMAX = 64 * 1024,
  };
  char *data;
  size_t datalen = 0;
  struct option opt;
  char socket_path[32];

  if ((data = malloc (DATAMAX)) == NULL)
    err (1, "malloc() failed");

  memset (&opt, 0, sizeof (opt));
  option_parse (&opt, argc, argv);
  argc -= optind;
  argv += optind;

  snprintf (socket_path, sizeof (socket_path), "/tmp/poked-%ld.ipc",
            (long)getuid ());
  fd = poke_connect (argc > 1 ? argv[1] : socket_path, opt.role);
  assert (fd != -1);

  data[0] = '\0';
  while ((n = read (0, data + datalen, DATAMAX - datalen)) != 0)
    if (n == -1)
      err (1, "read failed");
    else
      datalen += n;

  {
    uint16_t len16 = datalen;
    uint8_t len16le[2] = { len16, len16 >> 8 };

    if (!opt.raw_p)
      write_n_bytes (fd, (void *)len16le, sizeof (len16le));
    write_n_bytes (fd, data, len16);
  }

  close (fd);
  free (data);
  return 0;
}
