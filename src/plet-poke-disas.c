/* plet-poke-disas.c - A pokelet for Poke disas channel.  */

/* Copyright (C) 2022-2023 Mohammad-Reza Nabipoor */

/* This file is part of pacme.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <assert.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <err.h>

#define ENSURE(cond, msg)                                                     \
  do                                                                          \
    {                                                                         \
      if (!(cond))                                                            \
        errx (1, "condition '%s' failed: %s", #cond, msg);                    \
    }                                                                         \
  while (0)

static void
read_n_bytes (int fd, void *mem, size_t len)
{
  ssize_t n;
  size_t off = 0;

  while (off != len)
    {
      n = read (fd, mem + off, len - off);
      if (n == 0)
        errx (1, "EOF");
      if (n == -1)
        err (1, "read() failed");
      off += n;
    }
}

static int
poke_connect (const char *path, uint8_t role)
{
  int fd = socket (AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un adr;

  if (fd == -1)
    err (1, "socket() failed");
  memset (&adr, 0, sizeof (adr));
  adr.sun_family = AF_UNIX;
  snprintf (adr.sun_path, sizeof (adr.sun_path), path);
  if (connect (fd, (struct sockaddr *)&adr, sizeof (adr)) == -1)
    err (1, "connect(%s) failed", path);

  if (write (fd, &role, 1) != 1)
    err (1, "write(role) failed");

  return fd;
}

#define PDISAS_ITER_BEGIN 1
#define PDISAS_ITER_END 2
#define PDISAS_ERR 3
#define PDISAS_TXT 4
#define PDISAS_KIND 5

static void
process (char *mem, size_t len)
{
  static size_t i = 0;
  static const char *nl = "";
  static uint64_t n_iteration = 0;

  if (mem[0] == PDISAS_ITER_BEGIN)
    {
      ENSURE (len >= /*maybe one more extra '\0'*/ 1 + 8,
              "inconsistency in ITER_BEGIN");

      uint8_t *p = (uint8_t *)mem + 1;
      uint64_t idx = (uint64_t)p[7] << 56 | (uint64_t)p[6] << 48
                     | (uint64_t)p[5] << 40 | (uint64_t)p[4] << 32
                     | (uint64_t)p[3] << 24 | (uint64_t)p[2] << 16
                     | (uint64_t)p[1] << 8 | (uint64_t)p[0] << 0;
      ENSURE (n_iteration != idx,
              "inconsistency in iteration number (ITER_BEGIN)");
      n_iteration = idx;
      printf ("//--- %" PRIu64 "\n", idx);
      nl = "";
    }
  else if (mem[0] == PDISAS_TXT)
    {
      --len;
      ++mem;
      nl = len > 1 && mem[len - 2] == '\n' ? "" : "\n";
      printf ("%.*s", (int)len, mem);
      --mem;
    }
  else if (mem[0] == PDISAS_ITER_END)
    {
      ENSURE (len >= /*maybe one more extra '\0'*/ 1 + 8,
              "inconsistency in ITER_END");

      uint8_t *p = (uint8_t *)mem + 1;
      uint64_t idx = (uint64_t)p[7] << 56 | (uint64_t)p[6] << 48
                     | (uint64_t)p[5] << 40 | (uint64_t)p[4] << 32
                     | (uint64_t)p[3] << 24 | (uint64_t)p[2] << 16
                     | (uint64_t)p[1] << 8 | (uint64_t)p[0] << 0;
      if (n_iteration == 0)
        n_iteration = idx;
      else
        ENSURE (n_iteration == idx,
                "inconsistency in iteration number (ITER_END)");
      printf (nl);
    }
  else if (mem[0] == PDISAS_ERR)
    {
      --len;
      ++mem;
      nl = len > 1 && mem[len - 2] == '\n' ? "" : "\n";
      printf ("(error) %.*s", (int)len, mem);
      --mem;
    }
  fflush (stdout);
  ++i;
}

static void
usage (int new_line_p)
{
  if (new_line_p)
    putchar ('\n');
  puts ("Usage: plet-poke-disas [-h]");
  puts ("\nOptions:");
  puts ("  -h         Show this help message.");
}

int
main (int argc, char *argv[])
{
  int fd;

  {
    int opt;

    while ((opt = getopt (argc, argv, "h")) != -1)
      switch (opt)
        {
        case 'h':
          usage (0);
          exit (0);
          break;
        default:
          usage (1);
          exit (1);
        }
    argc -= optind;
    argv += optind;
  }

  {
    char socket_path[32];

    snprintf (socket_path, sizeof (socket_path), "/tmp/poked-%ld.ipc",
              (long)getuid ());
    fd = poke_connect (argc > 1 ? argv[1] : socket_path,
                       0x80 | /*USOCK_CHAN_OUT_PDISAS*/ 3);
    assert (fd != -1);
  }

  while (1)
    {
      uint8_t buf[2];
      size_t len;
      char *mem;

      read_n_bytes (fd, (void *)buf, 2);
      len = (size_t)buf[1] << 8 | buf[0];
      mem = malloc (len);
      if (mem == NULL)
        err (1, "malloc() failed");
      read_n_bytes (fd, mem, len);
      ENSURE (len > 1, "incosistency in received payload");
      process (mem, len);
      free (mem);
    }

  close (fd);
  return 0;
}
