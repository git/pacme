# tmux.conf -- tmux configuration file for pacme interface

# Copyright (C) 2022-2023 Mohammad-Reza Nabipoor

# This file is part of pacme.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

unbind C-b
set -g prefix C-g

# setw -g mouse on

bind O splitw \; send plet-out Enter \; selectp -t:.-
bind -T copy-mode e send-keys -X pipe-and-cancel plet-in
bind -T copy-mode E send-keys -X pipe-and-cancel 'sed "s/$/;/" | plet-in -c 2'

bind h selectp -L
bind j selectp -D
bind k selectp -U
bind l selectp -R

bind -r H resizep -L 3
bind -r J resizep -D 3
bind -r K resizep -U 3
bind -r L resizep -R 3

# Layout 1
bind F1 {
  send -l $EDITOR ' # $EDITOR'
  splitw -l 20%
  send plet-out Enter
  selectp -t:.-
  splitw -l 20%
  send 'plet-repl -p' Enter
  selectp -t:.-
  send Enter
}

# Layout 2
bind F2 {
  send -l 'plet-repl -p'
  splitw -l 20%
  send plet-out Enter
  selectp -t:.-
  # splitw -l 20%
  # send plet-vu Enter
  # selectp -t:.-
  send Enter
}

bind R {
  source-file @PACMEDATADIR@/tmux.conf

  source-file -q ~/.config/pacme/pacme.conf
  if-shell "test x$XDG_CONFIG_HOME != x" {
    source-file -q $XDG_CONFIG_HOME/pacme/pacme.conf
  }
  source-file -q ~/.pacme.conf
}
source-file -q ~/.config/pacme/pacme.conf
if-shell "test x$XDG_CONFIG_HOME != x" {
  source-file -q $XDG_CONFIG_HOME/pacme/pacme.conf
}
source-file -q ~/.pacme.conf
